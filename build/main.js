require('source-map-support/register')
module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./config.js":
/*!*******************!*\
  !*** ./config.js ***!
  \*******************/
/*! no static exports found */
/***/ (function(module, exports) {

const env = process.env;
const config = {
  secret: 'codinizfuuuuun'
};
module.exports = config;

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! express */ "express");
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var dotenv_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! dotenv/config */ "dotenv/config");
/* harmony import */ var dotenv_config__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(dotenv_config__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _routes_user__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./routes/user */ "./src/routes/user.js");
/* harmony import */ var cors__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! cors */ "cors");
/* harmony import */ var cors__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(cors__WEBPACK_IMPORTED_MODULE_3__);




const app = express__WEBPACK_IMPORTED_MODULE_0___default()();
const PORT = process.env.PORT || 4000;
console.log(process.env);
app.use(cors__WEBPACK_IMPORTED_MODULE_3___default()());
app.use(express__WEBPACK_IMPORTED_MODULE_0___default.a.json());
app.use(express__WEBPACK_IMPORTED_MODULE_0___default.a.urlencoded({
  extended: false
}));
app.use(cors__WEBPACK_IMPORTED_MODULE_3___default()());
app.use("/users", _routes_user__WEBPACK_IMPORTED_MODULE_2__["default"]);
app.listen(PORT, () => {
  console.log(`ciao sur le port ${PORT}`);
});

/***/ }),

/***/ "./src/models/user-model.js":
/*!**********************************!*\
  !*** ./src/models/user-model.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! mongoose */ "mongoose");
/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(mongoose__WEBPACK_IMPORTED_MODULE_0__);

const Schema = mongoose__WEBPACK_IMPORTED_MODULE_0___default.a.Schema;
const userSchema = new Schema({
  username: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  firstname: String,
  lastname: String,
  birthdate: Date
});
const User = mongoose__WEBPACK_IMPORTED_MODULE_0___default.a.model("User", userSchema);
/* harmony default export */ __webpack_exports__["default"] = (User);

/***/ }),

/***/ "./src/routes/user.js":
/*!****************************!*\
  !*** ./src/routes/user.js ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! express */ "express");
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var bcrypt__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! bcrypt */ "bcrypt");
/* harmony import */ var bcrypt__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(bcrypt__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var jsonwebtoken__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jsonwebtoken */ "jsonwebtoken");
/* harmony import */ var jsonwebtoken__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jsonwebtoken__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../config */ "./config.js");
/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_config__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _models_user_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../models/user-model */ "./src/models/user-model.js");
/* harmony import */ var mysql__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! mysql */ "mysql");
/* harmony import */ var mysql__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(mysql__WEBPACK_IMPORTED_MODULE_5__);



const UserRouter = express__WEBPACK_IMPORTED_MODULE_0___default.a.Router();


 // Date functions to convert JS date into mysql format

function twoDigits(d) {
  if (0 <= d && d < 10) return "0" + d.toString();
  if (-10 < d && d < 0) return "-0" + (-1 * d).toString();
  return d.toString();
}

Date.prototype.toMysqlFormat = function () {
  return this.getUTCFullYear() + "-" + twoDigits(1 + this.getUTCMonth()) + "-" + twoDigits(this.getUTCDate()) + " " + twoDigits(this.getUTCHours()) + ":" + twoDigits(this.getUTCMinutes()) + ":" + twoDigits(this.getUTCSeconds());
}; // Mysql connection and database/tables creation if do not exist


let host, user, password, db;

if (false) {} else {
  host = 'localhost';
  user = 'admin';
  password = '=@!#254tecmint';
  db = 'cprohprojet3';
}

const connection = mysql__WEBPACK_IMPORTED_MODULE_5___default.a.createConnection({
  host,
  user,
  password,
  multipleStatements: true
});
connection.connect();
connection.query('CREATE DATABASE IF NOT EXISTS ' + db, function (error, results, fields) {
  if (error) throw error;
  console.log('Database ' + db + ' created');
});
connection.query('CREATE TABLE IF NOT EXISTS ' + db + '.user (	userid int PRIMARY KEY NOT NULL AUTO_INCREMENT,	username varchar(255) NOT NULL,	password varchar(255) NOT NULL, createdat datetime, firstname varchar(255), lastname varchar(255), avatarurl varchar(255), addressid int, birthday datetime, UNIQUE(userid))', function (error, results, fields) {
  if (error) throw error;
  console.log('Table users created');
});
connection.query('CREATE TABLE IF NOT EXISTS ' + db + '.address (	addressid int PRIMARY KEY NOT NULL AUTO_INCREMENT,	city varchar(255) NOT NULL,	country varchar(255) NOT NULL, postalcode int NOT NULL)', function (error, results, fields) {
  if (error) throw error;
  console.log('Table adress created');
}); // Authentification functions

function generate_token(user) {
  const payload = {
    userid: user.userid
  };
  return jsonwebtoken__WEBPACK_IMPORTED_MODULE_2___default.a.sign(payload, _config__WEBPACK_IMPORTED_MODULE_3___default.a.secret);
}

function checkAuth(req, res, next) {
  try {
    const decoded = jsonwebtoken__WEBPACK_IMPORTED_MODULE_2___default.a.verify(req.body.token, _config__WEBPACK_IMPORTED_MODULE_3___default.a.secret);

    if (decoded.userid != req.params.id) {
      throw new Error('You don\'t have the right to access to this user data');
    }

    next();
  } catch (error) {
    return res.status(401).json({
      message: error.message
    });
  }
} // Routes
// Auth module


UserRouter.post('/login', (req, res) => {
  const username = req.body.username;
  connection.query('SELECT * FROM ' + db + '.user WHERE username = "' + username + '";', function (error, results, fields) {
    if (error) throw error;
    let isPasswordValid = false;

    if (results[0]) {
      const isPasswordValid = bcrypt__WEBPACK_IMPORTED_MODULE_1___default.a.compareSync(req.body.password, results[0].password);

      if (isPasswordValid) {
        res.status(200).send(generate_token(results[0]));
      } else res.status(403).send('Invalid password');
    } else {
      res.status(403).send('Invalid username');
    }
  });
}); // Users module

UserRouter.get('/', (req, res) => {
  connection.query('SELECT * FROM ' + db + '.user;', function (error, results, fields) {
    if (error) throw error;
    res.status(200).json(results);
  });
});
UserRouter.get('/:id', checkAuth, (req, res) => {
  connection.query('SELECT * FROM ' + db + '.user WHERE userid = "' + req.params.id + '";', function (error, results, fields) {
    if (error) throw error;
    res.status(200).json(results);
  });
});
UserRouter.post('/', (req, res) => {
  // Verification of username and password presence
  if (req.body.username && req.body.password) {
    // Verification of format of username as an email address
    if (/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(String(req.body.username).toLowerCase())) {
      // Verification that the username does not already exist
      let username_count = 0;
      connection.query('SELECT COUNT(*) FROM  ' + db + '.user WHERE username= "' + req.body.username + '";', function (error, results, fields) {
        if (error) throw error;
        username_count = results[0]['COUNT(*)'];

        if (username_count == 0) {
          // 1. we create first an empty address row to get the addressid
          connection.query('INSERT INTO ' + db + '.address (city, country, postalcode) VALUES ("", "", 0);', function (error, results, fields) {
            if (error) throw error; // 2. we create the user with the newly created addressid

            const salt = bcrypt__WEBPACK_IMPORTED_MODULE_1___default.a.genSaltSync(10);
            const hash = bcrypt__WEBPACK_IMPORTED_MODULE_1___default.a.hashSync(req.body.password, salt);
            const createdat = new Date().toMysqlFormat();
            connection.query('INSERT INTO ' + db + '.user (username, password, createdat, addressid) VALUES ("' + req.body.username + '", "' + hash + '", "' + createdat + '", "' + results.insertId + '");', function (error2, results2, fields2) {
              if (error2) throw error2;
              res.status(200).json({
                "result": "success",
                "msg": "User has successfully created"
              });
            });
          });
        } else {
          res.status(200).json({
            "result": "error",
            "msg": "This username already exists"
          });
        }
      });
    } else {
      res.status(200).json({
        "result": "error",
        "msg": "Username must be a valid email address"
      });
    }
  } else {
    res.status(200).json({
      "result": "error",
      "msg": "Username and password are required"
    });
  }
});
UserRouter.put('/:id', checkAuth, (req, res) => {
  // 1. we update the user data
  const salt = bcrypt__WEBPACK_IMPORTED_MODULE_1___default.a.genSaltSync(10);
  const hash = bcrypt__WEBPACK_IMPORTED_MODULE_1___default.a.hashSync(req.body.password, salt);
  const firstname = req.body.firstname || "";
  const lastname = req.body.lastname || "";
  const avatarurl = req.body.avatarurl || "";
  const birthday = req.body.birthday || "0000-00-00 00:00:00";
  connection.query('UPDATE ' + db + '.user SET username = "' + req.body.username + '", password = "' + hash + '", firstname = "' + firstname + '", lastname = "' + lastname + '", avatarurl = "' + avatarurl + '" WHERE userid = "' + req.params.id + '";', function (error, results, fields) {
    if (error) throw error; // 2. we get the address id

    connection.query('SELECT * FROM ' + db + '.user WHERE userid = "' + req.params.id + '";', function (error, results, fields) {
      if (error) throw error;
      const city = req.body.city || "";
      const country = req.body.country || "";
      const postalcode = req.body.postalcode || 0;
      const addressid = results[0].addressid; // 3. we update the address data

      connection.query('UPDATE ' + db + '.address SET city = "' + city + '", country = "' + country + '", postalcode = "' + postalcode + '" WHERE addressid = "' + addressid + '";', function (error, results, fields) {
        if (error) throw error;
        res.status(200).json('Updated user');
      });
    });
  });
});
UserRouter.delete('/:id', checkAuth, (req, res) => {
  connection.query('DELETE FROM ' + db + '.user WHERE userid = "' + req.params.id + '";', function (error, results, fields) {
    if (error) throw error;
    res.status(200).json('nothing');
  });
});
/* harmony default export */ __webpack_exports__["default"] = (UserRouter);

/***/ }),

/***/ 0:
/*!****************************!*\
  !*** multi ./src/index.js ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/matthieu/Desktop/node-react_apirest_users/back/src/index.js */"./src/index.js");


/***/ }),

/***/ "bcrypt":
/*!*************************!*\
  !*** external "bcrypt" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("bcrypt");

/***/ }),

/***/ "cors":
/*!***********************!*\
  !*** external "cors" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("cors");

/***/ }),

/***/ "dotenv/config":
/*!********************************!*\
  !*** external "dotenv/config" ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("dotenv/config");

/***/ }),

/***/ "express":
/*!**************************!*\
  !*** external "express" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("express");

/***/ }),

/***/ "jsonwebtoken":
/*!*******************************!*\
  !*** external "jsonwebtoken" ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("jsonwebtoken");

/***/ }),

/***/ "mongoose":
/*!***************************!*\
  !*** external "mongoose" ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("mongoose");

/***/ }),

/***/ "mysql":
/*!************************!*\
  !*** external "mysql" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("mysql");

/***/ })

/******/ });
//# sourceMappingURL=main.map