import express from "express"
import "dotenv/config"
import UserRouter from './routes/user'
import cors from 'cors'

const app =  express()

const PORT = process.env.PORT || 4000; 
console.log('Environment:', process.env.NODE_ENV);

app.use(cors());

app.use(express.json()); 
app.use(express.urlencoded({ extended: false }));

app.use("/users", UserRouter); 

app.listen(PORT, () => {console.log(`ciao sur le port ${PORT}`)})
