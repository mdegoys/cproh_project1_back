import express from 'express'
import bcrypt  from 'bcrypt';
import jwt from 'jsonwebtoken';
const UserRouter = express.Router();

import config from '../../config';
import mysql from 'mysql'

// Date functions to convert JS date into mysql format
function twoDigits(d) {
    if(0 <= d && d < 10) return "0" + d.toString();
    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
    return d.toString();
}

Date.prototype.toMysqlFormat = function() {
    return this.getUTCFullYear() + "-" + twoDigits(1 + this.getUTCMonth()) + "-" + twoDigits(this.getUTCDate()) + " " + twoDigits(this.getUTCHours()) + ":" + twoDigits(this.getUTCMinutes()) + ":" + twoDigits(this.getUTCSeconds());
};


// Mysql connection and database/tables creation if do not exist
let host, user, password, db;
if (process.env.NODE_ENV === 'production') {
	host = 'sql7.freemysqlhosting.net';
	user = 'sql7276871';
	password = 'HkR81LQAwd';
	db = 'sql7276871';
} else {
	host = 'localhost';
	user = 'admin';
	password = '=@!#254tecmint';
	db = 'cprohprojet3';
}

const connection = mysql.createConnection({ host, user, password, multipleStatements: true, dateStrings:true });
connection.connect();

connection.query('CREATE DATABASE IF NOT EXISTS '+db, function (error, results, fields) {
  if (error) throw error;
  console.log('Database '+db+' created or already existing.');
});

connection.query('CREATE TABLE IF NOT EXISTS '+db+'.user (	userid int PRIMARY KEY NOT NULL AUTO_INCREMENT,	username varchar(255) NOT NULL,	password varchar(255) NOT NULL, createdat datetime, firstname varchar(255), lastname varchar(255), avatarurl varchar(255), addressid int, birthday time, UNIQUE(userid))',
function (error, results, fields) {
  if (error) throw error;
  console.log('Table users created or already existing.');
});

connection.query('CREATE TABLE IF NOT EXISTS '+db+'.address (	addressid int PRIMARY KEY NOT NULL AUTO_INCREMENT,	city varchar(255) NOT NULL,	country varchar(255) NOT NULL, postalcode int NOT NULL)',
function (error, results, fields) {
  if (error) throw error;
  console.log('Table address created or already existing.');
});

// Authentification functions
function generate_token(user) {
	const payload = {
		userid: user.userid
	}
	return jwt.sign(payload, config.secret);
}

function checkAuth(req, res, next) {
	try {
		const decoded = jwt.verify(req.headers.token, config.secret) 
		if (decoded.userid != req.params.id) {
			throw new Error('You don\'t have the right to access to this user data.') 
		}
		next();
	} catch(error) {
		return res.status(401).json({
			'result': 'error', 'msg': error.message
		});
	}
}


// Routes
// Auth module
UserRouter.post('/login', (req, res)=> {

	const username = req.body.username;

	connection.query('SELECT * FROM '+db+'.user WHERE username = "'+username+'";',
	function (error, results, fields) {
		if (error) throw error;

		let isPasswordValid = false;

		if (results[0]) {
			const isPasswordValid = bcrypt.compareSync(req.body.password, results[0].password);
			if (isPasswordValid) {
				res.status(200).send({ 'result': 'success', 'token': generate_token(results[0]) });
			}
			else res.status(403).send({ 'result': 'error', 'msg': 'Invalid password' });
		}
		
		else {
			res.status(403).send({ 'result': 'error', 'msg': 'Invalid username' });
		}
		
	});


});


// Users module
UserRouter.get('/', (req, res)=> {

	connection.query('SELECT * FROM '+db+'.user;',
	function (error, results, fields) {
		if (error) throw error;
		res.status(200).json({ 'result': 'success', 'users': results })
	});

});

UserRouter.get('/:id', (req, res)=> {

	// 1. we get the basic data
	connection.query('SELECT * FROM '+db+'.user WHERE userid = "'+req.params.id+'";',
	function (error, results, fields) {
		if (error) throw error;

		// 2. we get the additionnal data and them to the response
		connection.query('SELECT * FROM '+db+'.address WHERE addressid = "'+results[0].addressid+'";',
		function (error2, results2, fields2) {
			if (error2) throw error2;

			const concatResults = Object.assign({}, results[0], results2[0]);

			res.status(200).json({ 'result': 'success', 'user': concatResults })
		});
	});

});

UserRouter.post('/', (req, res) => {

	// Verification of username and password presence
	if (req.body.username && req.body.password) {	
		// Verification of format of username as an email address
		if (/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(String(req.body.username).toLowerCase())) {
			// Verification that the username does not already exist
			let username_count = 0;
			connection.query('SELECT COUNT(*) FROM  '+db+'.user WHERE username= "'+req.body.username+'";',	function (error, results, fields) {
				if (error) throw error;
				username_count = results[0]['COUNT(*)'];

				if (username_count == 0) {

					// 1. we create first an empty address row to get the addressid
					connection.query('INSERT INTO '+db+'.address (city, country, postalcode) VALUES ("", "", 0);',
					function (error, results, fields) {
						if (error) throw error;

						// 2. we create the user with the newly created addressid
						const salt = bcrypt.genSaltSync(10);
						const hash = bcrypt.hashSync(req.body.password, salt);
						const createdat = new Date().toMysqlFormat();

						connection.query('INSERT INTO '+db+'.user (username, password, createdat, addressid) VALUES ("'+req.body.username+'", "'+hash+'", "'+createdat+'", "'+results.insertId+'");',
						function (error2, results2, fields2) {
							if (error2) throw error2;
							res.status(200).json({ "result": "success", "msg": "User has successfully created" })
						});

					});
				} else {
					res.status(200).json({ "result":"error", "msg": "This username already exists"});
				}
			});
			
			
		} else {
			res.status(200).json({ "result": "error", "msg": "Username must be a valid email address"});
		}
	} else {
		res.status(200).json({ "result": "error", "msg": "Username and password are required"});
	}
 
});

UserRouter.put('/:id', (req, res)=> {
	
	// 1. we update the user data
	const firstname = req.body.firstname || "";
	const lastname = req.body.lastname || "";
	const avatarurl = req.body.avatarurl || "";
	const birthday = null;
	// console.log(req.body.birthday);
	// date formats suck


	connection.query('UPDATE '+db+'.user SET firstname = "'+firstname+'", lastname = "'+lastname+'", avatarurl = "'+avatarurl+'", birthday = '+birthday+' WHERE userid = "'+req.params.id+'";',
	function (error, results, fields) {
		if (error) throw error;

		// 2. we get the address id
		connection.query('SELECT * FROM '+db+'.user WHERE userid = "'+req.params.id+'";',
		function (error, results, fields) {
			if (error) throw error;
			const city = req.body.city || "";
			const country = req.body.country || "";
			const postalcode = req.body.postalcode || 0;
			const addressid = results[0].addressid;
			

			// 3. we update the address data
			connection.query('UPDATE '+db+'.address SET city = "'+city+'", country = "'+country+'", postalcode = '+postalcode+' WHERE addressid = "'+addressid+'";',
			function (error, results, fields) {
				if (error) throw error;
				res.status(200).json({ 'result': 'success', 'msg': 'The  user information have been updated.' });		
			});
		});
	});
});

UserRouter.delete('/:id', (req, res)=>{
	
	//1. we get the adress is of the user
	connection.query('SELECT * FROM '+db+'.user WHERE userid = "'+req.params.id+'";',
		function (error, results, fields) {
			if (error) throw error;
			const addressid = results[0].addressid;
			
		//2. we delete the address
		connection.query('DELETE FROM '+db+'.address WHERE addressid = "'+addressid+'";',
		function (error, results, fields) {
			if (error) throw error;

			//3. we delete the user
			connection.query('DELETE FROM '+db+'.user WHERE userid = "'+req.params.id+'";',
			function (error2, results2, fields2) {
				if (error2) throw error2;
				res.status(200).json({ 'result': 'success', 'msg': 'The user has been deleted' })
			});
		});
	});
});

export default UserRouter;
